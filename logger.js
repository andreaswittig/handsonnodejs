define([], function() {
	"use strict";

	function log(level, message, data) {
		console.log("#" + process.pid + "(" + level + "): " + message, data);
	}

	return {
		debug: function(message, data) {
			log("debug", message, data);
		},
		error: function(message, data) {
			log("error", message, data);
		},
		exception: function(exception, data) {
			log("exception", exception.stack, data);
		}
	};
});