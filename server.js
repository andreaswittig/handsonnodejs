define(["config", "logger", "datastore", "socket.io", "redis", "node-static", "url", "http"],
	function(config, logger, datastore, socketio, redis, nodestatic, url, http) {
	"use strict";

	var staticServer;

	function onWebserverRequest(request, response) {
		logger.debug("Recieved server request");
		response.writeHead(200, {"Content-Type": "text/html; charset=UTF-8"});
		response.end();
	}

	function onStaticRequest(request, response) {
		request.addListener('end', function () {
			staticServer.serve(request, response);
		});
	}

	return {
		start: function(callback) {
			logger.debug("Starting server...");
			var webserver = http.createServer(function(request, response) {
				var pathname = url.parse(request.url).pathname;
				if ("/static/" === pathname.substr(0, 8)) {
					onStaticRequest(request, response);
				} else {
					onWebserverRequest(request, response);
				}
			});
			webserver.listen(8080);
			var websocketserver = socketio.listen(webserver, {
				store: new (socketio.RedisStore)({
					redisPub : redis.createClient(config.redisPort, config.redisHost, {}),
					redisSub : redis.createClient(config.redisPort, config.redisHost, {}),
					redisClient : redis.createClient(config.redisPort, config.redisHost, {})
				}),
				transports: ['websocket', 'flashsocket', 'xhr-polling']
			});
			websocketserver.sockets.on("connection", function (websocket) {
				logger.debug("Websocket client connected");
				datastore.getTweets(function(err, result) {
					if (err) {
						logger.debug("get tweets error", err);
					} else {
						result.forEach(function(tweet) {
							websocket.emit("tweet", tweet);
						});
					}
				});
				datastore.listenToTweets(function(err, tweet) {
					websocket.emit("tweet", tweet);
				});
				datastore.listenToChats(function(err, chat) {
					websocket.emit("chat", chat);
				});
				websocket.on("chat", function (data) {
					logger.debug("chat", data);
					datastore.addChat(data, function(err) {
						if (err) {
							logger.debug("add chat error", err);
						}
					});
				});
			});
			staticServer = new (nodestatic.Server)('.', {});
			callback(undefined);
		}
	};
});
