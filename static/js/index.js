var requireBonsai = requirejs.config({
	baseUrl: "/static/js/",
	paths: {
		"socketio": "lib/socketio/socket.io",
		"jqueryui": "lib/jquery-ui-1.8.22.custom.min",
		"bootstrap": "lib/bootstrap/bootstrap-2.0.4"
    },
	shim: {
		"socketio": {
			exports: "io"
		},
		"lib/bootstrap-button": {
			deps: ["bootstrap"]
		},
		"twitterstream": {
			deps: ["jqueryui", "bootstrap", "socketio", "lib/bootstrap/bootstrap-button", "lib/mustache"]
		},
		"require": {
			deps: ["socketio"]
		}
	}
});

requireBonsai(["twitterstream", "logger"], function(twitterstream, logger) {
	$(function() {
		logger.debug("load");
		$(function() {
			logger.debug("dom ready");
			twitterstream.init();
		});
	});
});
