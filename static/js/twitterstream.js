define(["logger"], function(logger) {

	var websocket;

	return {
		init: function() {
			logger.debug("init");

			websocket = io.connect("http://localhost:8080", {});
			websocket.on("tweet", function(data) {
				logger.debug("websocket tweet", data);
				var html = Mustache.render("<li><i>{{user}}</i>: {{text}}</li>", data);
				$("#tweets").html(html + $("#tweets").html());
				$("#all").html(html + $("#all").html());
			});

			websocket.on("chat", function(data) {
				logger.debug("websocket chat", data);
				var html = Mustache.render("<li><u>{{user}}</u>: {{text}}</li>", data);
				$("#chats").html(html + $("#chats").html());
				$("#all").html(html + $("#all").html());
			});

			$("#tabs").tab();
			$("#chatForm").submit(function() {
				var form = $(this);
				var user = form.find("[name=\"user\"]").val();
				var text = form.find("[name=\"text\"]").val();
				form.find("[name=\"text\"]").val("");
				websocket.emit("chat", {"user": user, "text": text});
				return false;
			});

			websocket.on("connecting", function(transportType) {
				logger.debug("websocket: connecting" + transportType);
			});
			websocket.on("connect", function() {
				logger.debug("websocket: connect");
			});
			websocket.on("disconnect", function() {
				logger.debug("websocket: disconnect");
			});
			websocket.on("connect_failed", function() {
				logger.debug("websocket: connect_failed");
			});
			websocket.on("error", function(e) {
				logger.debug("websocket: error", e);
			});
			websocket.on("reconnect_failed", function() {
				logger.debug("websocket: reconnect_failed");
			});
			websocket.on("reconnect", function(transportType, reconnectionAttempts) {
				logger.debug("websocket: reconnect");
			});
			websocket.on("reconnecting", function(reconnectionDelay, reconnectionAttempts) {
				logger.debug("websocket: reconnecting");
			});
		}
	};
});