define([], function() {

	function log(level, message, data) {
		if (window.console) {
			window.console.log(level + ": " + message, data);
		}
	}

	return {
		debug: function(message, data) {
			log("debug", message, data);
		},
		error: function(message, data) {
			log("error", message, data);
		}
	};
});
