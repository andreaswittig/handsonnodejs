define([], function() {
	"use strict";

	return {
		auth: "bcs5timeline:barcamp12",
		worker: 4,
		redisPort: 6379,
		redisHost: "localhost"
	};
});