define(["logger", "config", "redis", "events"], function(logger, config, redis, events) {
	"use strict";

	var redisClient = redis.createClient(config.redisPort, config.redisHost, {});
	redisClient.on("error", function(err) {
		logger.error("redis error: " + err);
		process.exit(2);
	});

	var redisClientSub = redis.createClient(config.redisPort, config.redisHost, {});
	redisClientSub.on("error", function(err) {
		logger.error("redis error: " + err);
		process.exit(2);
	});

	var emitter = new events.EventEmitter();
	redisClientSub.subscribe("tweets");
	redisClientSub.subscribe("chats");
	redisClientSub.on("message", function(channel, data) {
		try {
			data = JSON.parse(data);
			emitter.emit(channel, data);
		} catch(e) {
			logger.error("can not parse data as JSON", e);
		}
	});

	return {
		addTweet: function(tweet, callback) {
			var json = JSON.stringify(tweet);
			redisClient.RPUSH("tweets", json, function (err, res) {
				if (err) {
					callback(err);
				} else {
					callback(undefined);
				}
			});
			redisClient.publish("tweets", json);
		},
		addChat: function(chat, callback) {
			var json = JSON.stringify(chat);
			redisClient.publish("chats", json);
			callback(undefined);
		},
		getTweets: function(callback) {
			redisClient.LRANGE("tweets", -10, -1, function (err, res) {
				if (err) {
					callback(err, undefined);
				} else {
					var tweets = [];
					res.forEach(function(json, i) {
						var tweet = JSON.parse(json);
						tweets.push(tweet);
					});
					console.log(tweets);
					callback(undefined, tweets);
				}
			});
		},
		listenToTweets: function(callback) {
			emitter.on("tweets", function(tweet) {
				callback(undefined, tweet);
			});
		},
		listenToChats: function(callback) {
			emitter.on("chats", function(chat) {
				callback(undefined, chat);
			});
		}
	};
});