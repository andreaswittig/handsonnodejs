define(["logger", "config", "https", "datastore"], function(logger, config, https, datastore) {
	"use strict";

	var options = {
		host: 'stream.twitter.com',
		port: 443,
		path: '/1.1/statuses/filter.json?track=bcs5',
		method: 'GET',
		auth: config.auth
	};

	return {
		start: function(callback) {
			var request = https.request(options);
			request.on('response', function (response) {
				logger.debug("Started twitter stream");
				response.on('data', function (chunk) {
					try {
						var data = JSON.parse(chunk);
						var tweet  = {};
						tweet.user = data.user.screen_name;
						tweet.text = data.text;
						logger.debug("received data: " + tweet.text);
						datastore.addTweet(tweet, function(err) {
							if (err) {
								logger.error("Error while saving tweet.");
							}
						});
					} catch (e) {
						
					} 
				});
			});
			request.on("error", function(err) {
				logger.error("Could not open twitter stream" + err);
			});
			request.end();
			callback(undefined);
		},
		stop: function(callback) {
			callback(undefined);
		}
	};
});