var requirejs = require("requirejs");
requirejs.config({
	nodeRequire: require
});

require = requirejs;

requirejs(["logger", "server", "config", "twitterreceiver", "cluster"], function(logger, server, config, twitterreceiver, cluster) {
	"use strict";
	var i;

	function startWorker() {
		logger.debug("starting worker");
		cluster.fork();
	}

	if (cluster.isMaster) {
		twitterreceiver.start(function(err) {
			if (err) {
				logger.error("can not start twitterstream " + err);
				process.exit(1);
			} else {
				logger.debug("started twitterstream");
				cluster.on("exit", function(worker, code, signal) {
					logger.debug("worker exited: restart in 1 second...");
					setTimeout(function() {
						startWorker();
					}, 1000);
				});
				for (i = 0; i < config.worker; i = i+1) {
					startWorker();
				}
			}
		});
	} else {
		server.start(function(err) {
			if (err) {
				logger.error("can not start server: " + err);
				process.exit(1);
			}
		});
	}
});